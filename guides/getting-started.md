Get started with Angular Material using the Angular CLI.

## Install the CLI
 
 ```bash
 npm install -g angular-cli
 ```
 
## Create a new project
 
 ```bash
 ng new my-project
 ```

The new command creates a project with a build system for your Angular app.

## Install Seed Module's components 

```bash
npm install --save @angular/material
```

## Import the Seed Module NgModule
  
**src/app/app.module.ts**
```ts
import { SampleModule } from '@zenmom/seed';
// other imports 
@NgModule({
  imports: [SampleModule.forRoot()],
  ...
})
export class PizzaPartyAppModule { }
```

### Additional setup for gestures
Some components (`md-slide-toggle`, `md-slider`, `mdTooltip`) rely on 
[HammerJS](http://hammerjs.github.io/) for gestures. In order to get the full feature-set of these
components, HammerJS must be loaded into the application.

You can add HammerJS to your application via [npm](https://www.npmjs.com/package/hammerjs), a CDN 
(such as the [Google CDN](https://developers.google.com/speed/libraries/#hammerjs)), or served 
directly from your app.

#### If you want to include HammerJS from npm, you can install it:

```bash
npm install --save hammerjs 
```

After installing, import HammerJS on your app's module.
**src/app/app.module.ts**
```ts
import 'hammerjs';
```

## Configuring SystemJS
If your project is using SystemJS for module loading, you will need to add `@angular/material` 
to the SystemJS configuration:

```js
System.config({
  // existing configuration options
  map: {
    ...,
    '@zenmom/seed': 'npm:@zenmom/seed/bundles/seed.umd.js'
  }
});
```

## Sample Seed Module projects

