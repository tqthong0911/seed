import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MaterialModule } from '@angular/material';

import { SampleComponent } from './sample-component/index';
import { FlexLayoutModule } from '@angular/flex-layout';

import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    MaterialModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    FlexLayoutModule.forRoot()
    ],
  exports: [SampleComponent],
  declarations: [SampleComponent],
})
export class SeedModule {
  /** @deprecated */
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SeedModule,
      providers: []
    };
  }
}

