import {Routes} from '@angular/router';
import {Home} from './demo-app';

export const DEMO_APP_ROUTES: Routes = [
  {path: '', component: Home}
];
