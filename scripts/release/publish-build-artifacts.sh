#!/bin/bash

set -e -o pipefail

projectName="seed-module"
# Go to the project root directory
cd $(dirname $0)/../..

buildDir="dist/@zenmom/seed"
buildVersion=$(sed -nE 's/^\s*"version": "(.*?)",$/\1/p' package.json)

tempDir="tmp"

# Create a release of the current repository.
$(npm bin)/gulp build:release

# Prepare cloning the builds repository
rm -rf $tempDir
mkdir -p $tempDir

# Goto build directory.
cd $buildDir

# Create tar.gz Archive File
tagName="$projectName-$buildVersion"
tarFile="$tagName.tar.gz"
tar cvzf ../../../$tempDir/$tarFile .

# Go to the project root directory
cd ../../../

# Deploy to host
$(npm bin)/gulp deploy

# Goto temp directory.
cd $tempDir

projectId="$(curl -X GET \
   -H "PRIVATE-TOKEN:xjCaNy5wkjKaymvMABsF" \
 'http://git.zentek.vn/api/v3/projects' | jq '.[] | select(.name == "'$projectName'") | .id')"

echo "Project-$projectId"

description="$(curl -X POST \
   -H "PRIVATE-TOKEN:o7j2NpxjMtnq3if14Z3n" \
   -H "Content-Type:multipart/form-data" \
   -F "file=@\"./$tarFile\";type=image/jpeg;filename=\"$tarFile\"" \
 'http://git.zentek.vn/api/v3/projects/'$projectId'/uploads' | jq .markdown)"

curl -i -X POST \
   -H "Content-Type:application/json" \
   -H "PRIVATE-TOKEN:o7j2NpxjMtnq3if14Z3n" \
   -d \
'{
  "tag_name": "'"${tagName}"'",
  "ref": "develop",
  "message": "test create tag",
  "release_description": '"${description}"'
}' \
 'http://git.zentek.vn/api/v3/projects/'$projectId'/repository/tags'

echo "Finished publishing build artifacts"
