import { Component } from '@angular/core';
import { MdSnackBar } from '@angular/material';

import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

export interface IMaterialProperty {
  id?: any;
  name: any;
  language: any;
  uom: any;
  value: any;
  description: any;
}
/**
 * Sample Component.
 */
@Component({
  moduleId: module.id,
  selector: 'sample-component',
  templateUrl: 'sample-component.html',
  styleUrls: ['sample-component.css']
})
export class SampleComponent {
  complexForm: FormGroup;

  editing: any = {};
  languages = [
    { value: 'en', name: 'English' },
    { value: 'cn', name: 'China' },
    { value: 'vi', name: 'Vietnamese' },
    { value: 'jp', name: 'Japan' }
  ];

  rows: any[] = [];
  selected: any[] = [];
  temp: any[] = [];
  columns: any[] = [];

  model: IMaterialProperty = {
    id: '',
    name: '',
    language: '',
    uom: '',
    value: '',
    description: ''
  };
  buttonName = 'Cretate';
  isLoading = true;

  formError = {
    name: '',
    message: ''
  };

  convertDataLocalToDataWeb(item: any) {
  }

  init() {
    for (var i = 0; i < this.rows.length; i++) {
      this.rows[i].translate = this.rows[i].language[0];
    }
  }
  submitForm(form: any) {
    console.log(form);
    console.log(form.value);

  }

  showRequired(value: any) {
    // if (value == null  value.length < 0) {
    //   return 
    // } else {

    // }
    return false;
  }
  constructor(public snackBar: MdSnackBar, fb: FormBuilder) {
    this.fetch((data: any) => {
      // cache our list
      this.temp = [...data];
      // push our inital complete list
      this.rows = data;
      this.init();

      this.complexForm = fb.group({
        'id': [null, Validators.required],
        'value': [null, Validators.required],
        'description': [null, Validators.required],
        'uom': [null, Validators.required]
      });
      this.complexForm.valueChanges.subscribe((form: any) => {
        console.log('form changed to:', form);
      });
    });
  }

  ngOnInit() {
    this.columns = [
      { prop: 'id', name: 'ID', maxWidth: 140},
      { prop: 'name', name: 'Name'},
      { prop: 'translate', name: 'Language', maxWidth: 100},
      { prop: 'uom', name: 'UOM', maxWidth: 80},
      { prop: 'value', name: 'Value'},
      { prop: 'description', name: 'Description'}
    ];
  }

  openSnackBar() {
    // this.snackBar.open('message', 'true', {
    //   duration: 2000,
    //   viewContainerRef: this.idToolbar
    // });
  }


  fetch(cb: any) {
    cb([{
      'id': '0123456789ABC',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'mm',
      'value': '{4.85...5.15}',
      'description': '5 millimeter sheet'
    }, {
      'id': '0123456789DEF',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'Pa-s',
      'value': '{250x10^3...255x10^3}',
      'description': 'Coefficient of viscosity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'pH',
      'value': '{3.99...4.01}',
      'description': 'Acidity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'g/m2',
      'value': '{20...21}',
      'description': 'Weight to be added to shipping label'
    }, {
      'id': '0123456789ABC',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'mm',
      'value': '{4.85...5.15}',
      'description': '5 millimeter sheet'
    }, {
      'id': '0123456789DEF',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'Pa-s',
      'value': '{250x10^3...255x10^3}',
      'description': 'Coefficient of viscosity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'pH',
      'value': '{3.99...4.01}',
      'description': 'Acidity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'g/m2',
      'value': '{20...21}',
      'description': 'Weight to be added to shipping label'
    }, {
      'id': '0123456789ABC',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'mm',
      'value': '{4.85...5.15}',
      'description': '5 millimeter sheet'
    }, {
      'id': '0123456789DEF',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'Pa-s',
      'value': '{250x10^3...255x10^3}',
      'description': 'Coefficient of viscosity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'pH',
      'value': '{3.99...4.01}',
      'description': 'Acidity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'g/m2',
      'value': '{20...21}',
      'description': 'Weight to be added to shipping label'
    }, {
      'id': '0123456789ABC',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'mm',
      'value': '{4.85...5.15}',
      'description': '5 millimeter sheet'
    }, {
      'id': '0123456789DEF',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'Pa-s',
      'value': '{250x10^3...255x10^3}',
      'description': 'Coefficient of viscosity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'pH',
      'value': '{3.99...4.01}',
      'description': 'Acidity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'g/m2',
      'value': '{20...21}',
      'description': 'Weight to be added to shipping label'
    }, {
      'id': '0123456789ABC',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'mm',
      'value': '{4.85...5.15}',
      'description': '5 millimeter sheet'
    }, {
      'id': '0123456789DEF',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'Pa-s',
      'value': '{250x10^3...255x10^3}',
      'description': 'Coefficient of viscosity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'pH',
      'value': '{3.99...4.01}',
      'description': 'Acidity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'g/m2',
      'value': '{20...21}',
      'description': 'Weight to be added to shipping label'
    }, {
      'id': '0123456789ABC',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'mm',
      'value': '{4.85...5.15}',
      'description': '5 millimeter sheet'
    }, {
      'id': '0123456789DEF',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'Pa-s',
      'value': '{250x10^3...255x10^3}',
      'description': 'Coefficient of viscosity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'pH',
      'value': '{3.99...4.01}',
      'description': 'Acidity'
    }, {
      'id': '0123456789GHI',
      'name': 'Material property definition',
      'language': 'en',
      'uom': 'g/m2',
      'value': '{20...21}',
      'description': 'Weight to be added to shipping label'
    }
    ]);
  };
  lengthStringRow = {
    id: 10,
    name: 20,
    uom: 20,
    value: 15,
    description: 20,
    language: 10
  };
  showAction = false;
  updateFilter(event: any) {
    let val = event.target.value;

    // filter our data
    let temp = this.temp.filter(function (d) {
      return d.id.toString().toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
  };

  updateValue(event: any, cell: any, cellValue: any, row: any) {
    this.editing[row.$$index + '-' + cell] = false;
    this.rows[row.$$index][cell] = event.target.value;
  };

  create(sidenav: any, model: IMaterialProperty) {
    console.log(model);
    this.openSnackBar();
    sidenav.close();

  }

  resetModel() {
    this.buttonName = 'Cretate';
    this.model = {
      id: '',
      name: '',
      language: '',
      uom: '',
      value: '',
      description: ''
    };
  }

  showNavCreate(sidenav: any) {
    this.resetModel();
    sidenav.open();
  }

  reset() {
    this.resetModel();
    this.selected = [];
  }

  onSelect(sidenav: any, { selected }: any) {
    if (selected.length <= 0) {
      return;
    }
    this.buttonName = 'Update';
    this.model = selected[selected.length - 1];
    this.selected = [];
    this.selected.push(selected[selected.length - 1]);
    sidenav.open();
    this.isLoading = false;
  }

  onActivate(event: any) {

  }



  openSidenav(event: any) {
    console.log('openSidenav');
  }
}
