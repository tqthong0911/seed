import {NgModule} from '@angular/core';
import {BrowserModule, AnimationDriver} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {E2EApp, Home} from './e2e-app/e2e-app';
import {E2E_APP_ROUTES} from './e2e-app/routes';

import { SeedModule } from '@zenmom/seed';
import {SampleComponentE2E} from './sample-component/sample-component-e2e';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(E2E_APP_ROUTES),
    SeedModule.forRoot(),
  ],
  declarations: [
    E2EApp,
    Home,

    SampleComponentE2E
  ],
  bootstrap: [E2EApp],
  providers: [
    {provide: AnimationDriver, useValue: AnimationDriver.NOOP},
  ],
  entryComponents: [E2EApp]
})
export class E2eAppModule { }
