import {Component, ViewEncapsulation, ElementRef} from '@angular/core';


@Component({
  selector: 'home',
  template: `
    <sample-component></sample-component>
  `
})
export class Home {}

@Component({
  moduleId: module.id,
  selector: 'demo-app',
  providers: [],
  templateUrl: 'demo-app.html',
  styleUrls: ['demo-app.css'],
  encapsulation: ViewEncapsulation.None,
})
export class DemoApp {
  navItems = [
    {name: 'Button', route: 'button'}
  ];

  constructor(private _element: ElementRef) {

  }
}
