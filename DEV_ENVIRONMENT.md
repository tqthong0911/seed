# Developer guide

## Set up your gitlab runner environment

```shell
sudo apt-get install jq -y
npm install -g gulp-cli
```

## Getting your environment set up

1. Make sure you have `node` installed with a version at _least_ 5.5.0.
2. Run `npm install -g gulp-cli gulp` to install gulp.
3. Clone Seed Module project.
4. From the root of the project, run `npm install`.


To build the components in dev mode, run `gulp build:components`.
To build the components in release mode, run `gulp build:release`
 
To bring up a local server, run `gulp serve:devapp`. This will automatically watch for changes 
and rebuild. The browser should refresh automatically when changes are made.

### Change Project Name

1. In 'package.json'.
   Change 'seed-module' => 'your project name with -'
2. In 'tools/gulp/constants.ts'.
   Change line ```export const DIST_COMPONENTS_ROOT = join(DIST_ROOT, '@zenmom/seed-module');```.

3. In 'src/demo-app/demo-app-module.ts'.
   Change line ```import {SeedModule} from '@zenmom/seed-module';```

4. In 'system-config.ts'.
   ```javascript
   @zenmom/seed-module': '@zenmom/seed-module/bundles/seed.umd.js
   ```

5. In 'tsconfig.json'
   Find all 'seed-module' => your project name

6. In 'system-config-spec.ts'
   Find all 'seed-module' => your project name

7. In 'tsconfig-srcs.json'
   Find all 'seed-module' => your project name

8. In 'tools/gulp/tasks/components'.
   'seed-module' => your project name

### Add new Component, Directive, Pipe, Service

1. Create a folder with component name. (Ex: sample-component).
   **NOTE** There is no blank, dot in component name.

2. You must have:
   a. `index.ts` export file.
   b. `*.html` for template. (just for Component).
   c. `*.scss` for style. (just for Component).
   d. `*.spec.ts` for test Component, Directive, Pipe, Service.
   e. `*.ts` for controller.

3. Export your Component, Directive, Pipe, Service in module.
   In file `index.ts`: Export your Component,... here.
   In file `module.ts`: add Component, Directive, Pipe in `exports` and `declarations`. add Service in `providers`

### Add 3rd Library

1. Run `npm install --save-dev @library`
2. In `src/demo-app/system-config.ts` add `'@swimlane/ngx-datatable': 'vendor/@swimlane/ngx-datatable/release/index.js'`
   in `map` array.
3. In `tools/gulp/constants.ts` add `, '@swimlane'` in `NPM_VENDOR_FILES` array
4. For **Angular Material**, open `tools/gulp/tasks/components.ts` add `'@angular/material': 'ng.material'` in `globals` in
   `':build:components:rollup'` task

### Running tests

To run unit tests, run `gulp test`.
To run the e2e tests, run `gulp e2e`.
To run lint, run `gulp lint`.


### Running benchmarks
Not yet implemented.

### Running screenshot diff tests
Not yet implemented.
