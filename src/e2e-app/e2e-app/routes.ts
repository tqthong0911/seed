import {Routes} from '@angular/router';
import {Home} from './e2e-app';
import {SampleComponentE2E} from '../sample-component/sample-component-e2e';

export const E2E_APP_ROUTES: Routes = [
  {path: '', component: Home},
  {path: 'sample-component', component: SampleComponentE2E},
];
